$(document).ready(function() {
    var curSlide = 1,
        totalSlides = document.getElementsByClassName("slide").length,
        info = document.querySelector(".info");
    function updateInfo() {
        info.textContent = "Slide " + curSlide + " / " + totalSlides;
    }
    function changeSlide(direction) {
        if (curSlide + direction < 1 || curSlide + direction > totalSlides || isNaN(+direction) || !direction) return;
        $(".slide").eq(curSlide - 1).fadeOut(200);
        curSlide += direction;
        $(".slide").eq(curSlide - 1).delay(200).fadeIn(200);
        updateInfo();
    }
    updateInfo();
    $(".slide").slice(1).hide();
    $(document).bind("contextmenu", function(e) {
        e.preventDefault();
    });
    $(document).click(function(e) {
        if ($(".slide").is(":animated")) return;
        e.preventDefault();
        changeSlide(2 - e.which);
    });
    $(document).keydown(function(e) {
        if ($(".slide").is(":animated")) return;
        if (e.which === 37 || e.which === 40)
            changeSlide(-1);
        else if (e.which === 38 || e.which === 39)
            changeSlide(1);
        else if (e.which === 80)
            changeSlide(+prompt("Which Slide Number?") - curSlide);
    });
});